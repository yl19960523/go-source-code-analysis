# 基础探究      
## map的整体结构        
Golang中的map的底层实现的是一个散列表，因此实现map的过程实际上就是实现散列表的过程。在这个散列表中最主要的两个结构一个叫hmap，另一个叫bucket。两种结构的样子如下：  
hmap    
![avater](map的整体结构.jpg)    
图中有很多字段，我们只需要关心标红的字段：bucket数组。Golang的map中用于存储的结构是bucket数组。bucket(bmap)的结构如下：     
bmap        
![avater](bucket的结构.jpg)     
我们使用的map中的key和value就存储在bmap的标红部分。“高位哈希值”数组记录的是当前bucket中key相关的索引，还有一个字段指向了扩容的bucket指针，这使的bucket会形成一个链表。          
![avater](bucket数组指针.jpg)       
由此可以看出hamp和bucket的关系如下：                
![avater](hmap和bucket的关系.jpg)           
哈希表的特点是会有个哈希函数，对于传进来的key进行哈希运算，得到唯一的值。map中也是有这样的一个哈希函数，也会算出一个值，对于值的使用golang是很有意思的。golang把求得的值按照用途一分为二：高位和低位        
![avater](map-hash.jpg)             
如图所示，蓝色为高位，红色为低位。然后低位用于寻找当前key属于map中的哪个bucket，而高位用于寻找bucket中的哪个key，bucket中有个属性字段是“高位哈希值”数组，这里存的就是蓝色的高位值，用来声明当前bucket中有哪些“key”，便于搜索查找。 需要特别指出的一点是：我们map中的key/value值都是存到同一个数组中的。数组中的顺序是这样的:            
![avater](bucket数组.jpg)           
所以map的整体结构如下：         
![avater](map&bucket.jpg)       
## map的扩容        
### 加载因子            
加载因子是一个阈值，一般表示为：散列包含的元素数 除以 位置总数。是一种“产生冲突机会”和“空间使用”的平衡与折中：加载因子越小，说明空间空置率高，空间使用率小，但是加载因子越大，说明空间利用率上去了，但是“产生冲突机会”高了。        
每种哈希表的都会有一个加载因子，数值超过加载因子就会为哈希表扩容。Golang的map的加载因子的公式是：map长度 / 2^B阈值是6.5。其中B可以理解为已扩容的次数。      
当Go的map长度增长到大于加载因子所需的map长度时，Go语言就会将产生一个新的bucket数组，然后把旧的bucket数组移到一个属性字段oldbucket中。注意：并不是立刻把旧的数组中的元素转义到新的bucket当中，而是，只有当访问到具体的某个bucket的时候，会把bucket中的数据转移到新的bucket中。
![avater](扩容处理.jpg)